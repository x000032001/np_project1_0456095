#pragma once
#include <sys/time.h>
#include <sys/resource.h>

#include "Logger.h"
#include "Message.h"
#include "NumberedPipe.h"
#include "Executor.h"

extern MessageCenter msgCenter;

enum StatusResult {
	ProcAllDone,
	ProcNotAllDone,
	ProcNotMine
};

class ProcessGrouper {
	public:
		string originCmds;

		ProcessGrouper(vector<Executor> exes) :
			executors(exes) {}

		int Start(NumberedPipeConfig,char**);
		int NotifyTerminated(pid_t);
		int PassSignal(int sig);
		pid_t GetPgid();

	private:
		vector<Executor> executors;
		pid_t pgid;
};
